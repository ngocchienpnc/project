import { _decorator, Animation, Component, EventMouse, Input, input, Node } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('Player')
export class Player extends Component {

    @property(Animation) anim: Animation;

    private currentAnimationName: string = 'stand_by';

    start(){
        this.playAnimation(this.currentAnimationName)
    }
    onClickGameA(){
        this.currentAnimationName = 'walk'
        this.playAnimation(this.currentAnimationName)
    }

    onClickGameB(){
        this.currentAnimationName = 'run'
        this.playAnimation(this.currentAnimationName)
    }

    playAnimation(animationName: string) {
        // Chạy animation với tên được đưa vào
        if (this.anim) {
            this.anim.getComponent(Animation).play(animationName);
        }
    }
}


