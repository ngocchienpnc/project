import { _decorator, Component, EventTouch, Input, input, math, Node, Vec2, Vec3, view } from 'cc';
import ICubismLookTarget from '../extensions/live2d_cubismsdk_cocoscreator/static/assets/Framework/LookAt/ICubismLookTarget';

const { ccclass, property } = _decorator;

@ccclass('CubismLookTarget')
export class CubismLookTarget extends Component {

    readonly [ICubismLookTarget.SYMBOL]: typeof ICubismLookTarget.SYMBOL = ICubismLookTarget.SYMBOL;

    private _position: math.Vec3 = Vec3.ZERO;

    getPosition(): math.Vec3 { return this._position; }

    isActive(): boolean { return true }
    start() {
        input.on(Input.EventType.TOUCH_MOVE, this.onTouchMove, this)
        input.on(Input.EventType.TOUCH_END, this.onTouchEnd, this)
    }

    onTouchMove(event: EventTouch) {
        console.log('a');
        
        let targetposition = event.getLocationInView();
        targetposition.x = targetposition.x / view.getVisibleSize().width
        targetposition.y = targetposition.y / view.getVisibleSize().height

        targetposition = targetposition.multiplyScalar(2).subtract(Vec2.ONE)
        console.log(targetposition);


        this._position = new Vec3(targetposition.x, targetposition.y, this._position.z)
        console.log(this._position);
        
    }

    onTouchEnd() {
        this._position = Vec3.ZERO;
    }
    update(deltaTime: number) {

    }
}