import { _decorator, Component, Node } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('NewComponent')
export class NewComponent extends Component {
    @property(Node)
    parentNode: Node = null;

    start() {
        // Lấy tất cả các node con của parentNode
        const childNodes = this.parentNode.children;

        // Sắp xếp các node con theo thứ tự giảm dần của tên
        childNodes.sort((a, b) => {
            const nameA = parseInt(a.name);
            const nameB = parseInt(b.name);
            return nameB - nameA;
        });

        // Đặt lại vị trí z để đảm bảo thứ tự hiển thị
        for (let i = 0; i < childNodes.length; i++) {
            childNodes[i].zIndex = i;
        }
    }
}


